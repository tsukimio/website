---
layout: home
nav_title: Home
---


<strong>つきみお<span> ( tsukimio )</span></strong> is a group of developers focused on making modifications to Android. This includes making ROMs for devices, making Magisk modules, and Android apps.

Our goal is to make all of our products free to use and licensed under the <a href="https://www.gnu.org/licenses/gpl-3.0-standalone.html"> GPL-3.0 license</a>, or similiar, to allow the source code and distrubutibles to be shared and modified by all.
